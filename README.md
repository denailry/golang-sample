# Build and Run
```bash
docker build -t name_of_image .
docker run -d -e "PORT=3000" -p 3000:3000 --name name_of_container name_of_image
```
Opening "localhost:3000" on browser will show a message.

# Fail and Success
To make **fail** commit, open "main_test.go" and make value of **value** differs from value of **expected**. To make **success** commit, do otherwise.