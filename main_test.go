package main

import "testing"

func TestMain(t *testing.T) {
	value := 1
	expected := 1

	if value != expected {
		t.Errorf("expected number is %d, but received value is %d", expected, value)
	}
}
