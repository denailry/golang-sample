FROM golang:alpine

ADD . /go/src/app
WORKDIR /go/src/app

EXPOSE 7000

CMD ["go", "run", "main.go"]